class LocalStorage:
	def __init__(self, accessToken=None, refreshToken=None, userInfo=None, driver=None):
		self.accessToken = accessToken
		self.refreshToken = refreshToken
		self.userInfo = userInfo
		self.driver = driver

	def userInfo(self):
		self.driver.execute_script(" return window.localStorage.getItem('userInfo')")

	def refreshToken(self):
		return self.driver.execute_script(" return window.localStorage.getItem('refreshToken')")

	def accessToken(self):
		return self.driver.execute_script(" return window.localStorage.getItem('accessToken')")
#
# {'clear': {}, 'getItem': {}, 'key': {}, 'length': 0, 'removeItem': {}, 'setItem': {}}
#
# {'accessToken': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MjA4LCJyb2xlIjoiVVNFUiIsImlhdCI6MTY0MjQ5MDA2NiwiZXhwIjoxNjQyNTA4MDY2fQ.FfsA5F-xX9ktSq5XfMWT3BVd3VQfzuxO-RExNfEhKUk',
# 'clear': {},
# 'getItem': {},
# 'key': {},
# 'length': 3,
# 'refreshToken': '{"token":"2e52e324-23c0-4107-bb60-f4f977003d64","expiresAt":"2022-01-19T07:14:26.921Z"}',
# 'removeItem': {},
# 'setItem': {},
# 'userInfo': '{"id":208,"name":"Evgen","surname":"Sichka","email":"sichkarenko96@gmail.com","emailConfirmed":true,"phone":"+380982542111","passwordRestoring":false,"countOfParkingPlaces":"12","balance":-900}'}