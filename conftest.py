import pytest
from selenium import webdriver
from selenium.webdriver.android.webdriver import WebDriver
from webdriver_manager.chrome import ChromeDriverManager


@pytest.fixture(scope="function")
def get_driver(request):
    driver: WebDriver = webdriver.Chrome(executable_path=ChromeDriverManager().install())
    driver.maximize_window()
    request.cls.driver = driver
    yield
    driver.quit()
