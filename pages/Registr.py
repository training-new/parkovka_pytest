from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class Registration:

	def __init__(self, driver):
		self.driver = driver

	def open(self):
		return self.driver.get("https://staging.parkovki.com.ua/")

	def registration_btn1(self):
		self.driver.find_element(By.CSS_SELECTOR, "#root .sc-kfYoZR.cJcgyR").click()

	def registration_btn2(self):
		self.driver.find_element(By.CSS_SELECTOR, ".sc-kfYoZR.fXPAJs").click()

	def first_name_input(self, username):
		self.driver.find_element(By.CSS_SELECTOR, "input[name='name']").clear()
		self.driver.find_element(By.CSS_SELECTOR, "input[name='name']").send_keys(username)

	def last_name_input(self, lastname):
		self.driver.find_element(By.CSS_SELECTOR, "input[name='surname']").clear()
		self.driver.find_element(By.CSS_SELECTOR, "input[name='surname']").send_keys(lastname)

	def number_input(self, phone):
		self.driver.find_element(By.CSS_SELECTOR, "input[name='phone']").clear()
		self.driver.find_element(By.CSS_SELECTOR, "input[name='phone']").send_keys(phone)

	def number_text(self):
		return self.driver.find_element(By.CSS_SELECTOR, "label[for='phone'] > p.sc-dIvrsQ.kPMqOL").get_attribute("textContent")

	def email_input(self, email):
		self.driver.find_element(By.CSS_SELECTOR, "input[name='email']").clear()
		self.driver.find_element(By.CSS_SELECTOR, "input[name='email']").send_keys(email)

	def password_input(self, password):
		self.driver.find_element(By.CSS_SELECTOR, "input[name='password']").clear()
		self.driver.find_element(By.CSS_SELECTOR, "input[name='password']").send_keys(password)

	def condition_checkbox(self):
		self.driver.find_element(By.CSS_SELECTOR, "input[name='checkbox']").click()

	def confirm_button(self):
		self.driver.find_element(By.XPATH, "//form//button[text()='Підтвердити телефон']").click()

	def number_attr(self):
		return WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.CSS_SELECTOR, "div.sc-dlnjwi.dJXsSm p.sc-eCApnc.iylGhi"))).get_attribute("textContent")

	def quit(self):
		self.driver.quit()

class Login:

	def __init__(self, driver):
		self.driver = driver

	def open(self):
		return self.driver.get('https://staging.parkovki.com.ua/')

	def Login_btn_1(self):
		self.driver.find_element(By.CSS_SELECTOR, "#root .sc-kfYoZR.cJcgyR").click()

	def Login_btn_2(self):
		self.driver.find_element(By.CSS_SELECTOR, ".sc-kfYoZR.fwQFNn").click()

	def Email_aut(self, email):
		self.driver.find_element(By.CSS_SELECTOR, "input[name='email']").clear()
		self.driver.find_element(By.CSS_SELECTOR, "input[name='email']").send_keys(email)

	def email_text(self):
		return WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.CSS_SELECTOR, "label[for='email'] p.sc-dIvrsQ.kPMqOL"))).get_attribute("textContent")
		#return self.driver.find_element(By.CSS_SELECTOR, "label[for='email'] p.sc-dIvrsQ.kPMqOL").get_attribute("textContent")

	def Password_input(self, password):
		self.driver.find_element(By.CSS_SELECTOR, "input[name='password']").clear()
		self.driver.find_element(By.CSS_SELECTOR, "input[name='password']").send_keys(password)

	def Login_conform_btn(self):
		self.driver.find_element(By.CSS_SELECTOR, "div.sc-dlnjwi.dJXsSm button:nth-of-type(1)").click()

	def quit(self):
		self.driver.quit()

	def asrt(self):
		#actual_result = self.driver.find_element(By.CSS_SELECTOR, "div.sc-bCwfaz.hzzSzX.sc-iwajpm.kydRHc")
		#return self.wait.until(lambda  x: x.find_element(By.CSS_SELECTOR, "div.sc-bCwfaz.hzzSzX.sc-iwajpm.kydRHc"))
		return WebDriverWait(self.driver, 10).until(EC.visibility_of_element_located((By.CSS_SELECTOR, "div.sc-bCwfaz.hzzSzX")))