from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from datetime import date, timedelta

class Booking:

	def __init__(self, driver):
		self.driver = driver

	def open(self):
		return self.driver.get("https://staging.parkovki.com.ua/")

	def calendar_start(self):
		today = date.today()
		d1 = today.strftime("%d.%m.%Y")
		self.driver.find_element(By.ID, "your_unique_start_date_id").send_keys(d1)

	def calendar_end(self):
		today = date.today() + timedelta(1)
		d1 = today.strftime("%d.%m.%Y")
		self.driver.find_element(By.ID, "your_unique_end_date_id").send_keys(d1)

	def search_parking_button(self):
		WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "button.sc-kfYoZR.kyetJz"))).click()

	def choose_parking(self):
		WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "#root > div.App > div > div.sc-uxdHp.fiDOnB > div.sc-biJonm.ernndx > div:nth-child(1) > div.sc-jgPyTC.fuJtcf"))).click()
		#self.driver.find_element(By.CSS_SELECTOR, "#root > div.App > div > div.sc-uxdHp.fiDOnB > div.sc-biJonm.ernndx > div:nth-child(1) > div.sc-jgPyTC.fuJtcf").click()

	def booking_btn(self):
		WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH,
		"/html/body/div[1]/div[2]/div/div[3]/div[1]/div[2]/div[2]/button"))).click()

	#(By.CSS_SELECTOR, ".sc-ellfGf.edxkse button.sc-kfYoZR.gHsZEk")
	# root > div.App > div > div.sc-bUQyIj.exIfBg > div.sc-cGKJhA.fTwFWY > div.sc-JsfZP.kBcPwt > div.sc-ellfGf.edxkse > button
	def input_name(self):
		self.driver.find_element(By.CSS_SELECTOR, "input[name='name']").send_keys("Петро")

	def input_brand_auto(self):
		self.driver.find_element(By.CSS_SELECTOR, "input[name='car']").send_keys("шкода")

	def input_color(self):
		self.driver.find_element(By.CSS_SELECTOR, "input[name='color']").send_keys("белій")

	def input_email(self):
		self.driver.find_element(By.CSS_SELECTOR,  "input[name='email']").send_keys("sichkarenko96@gmail.com")

	def input_phone_number(self):
		self.driver.find_element(By.CSS_SELECTOR,  "input[name='phoneNumber']").send_keys("+380982542111")

	def button_confirm_booking(self):
		self.driver.find_element(By.CSS_SELECTOR, "form button.sc-kfYoZR.dMcanQ").click()

	def requst_bokk_send(self):
		return WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "div.sc-dlnjwi.dJXsSm p.sc-crzoAE.DykGo"))).get_attribute("textContent")

	def button_close(self):
		self.driver.find_element(By.CSS_SELECTOR, ".sc-iCoGMd.kMthTr button").click()

	def quit(self):
		self.driver.quit()