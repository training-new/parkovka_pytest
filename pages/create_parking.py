import os.path
import random
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class Create_parking:

	def __init__(self, driver):
		self.driver = driver

	def open(self):
		return self.driver.get("https://staging.parkovki.com.ua/partnerprofile")

	def btn_add_parking(self):
		WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.XPATH, "/html/body/div[1]/div[2]/div[2]/div/div/div/div[2]"))).click()

	def select_options_city(self):
		#Select(self.driver.find_element(By.CSS_SELECTOR, "select[name='cityId']")).select_by_value('0')
		Select(WebDriverWait(self.driver, 5).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "select[name='cityId']")))).select_by_value('2')

	def input_street(self):
		self.driver.find_element(By.ID, "react-select-2-input").send_keys("Греческая6")
		WebDriverWait(self.driver, 5).until(EC.element_to_be_clickable((By.ID, "react-select-2-option-0"))).click()

	def input_house(self):
		self.driver.find_element(By.NAME, "house").send_keys("10")

	def input_numb_place(self):
		number = random.randint(1,150)
		self.driver.find_element(By.NAME, "placeNumber").send_keys(number)

	def input_lvl_parking(self):
		self.driver.find_element(By.NAME, "parkingLevel").send_keys("1")

	def input_width_place(self):
		self.driver.find_element(By.NAME, "placeWidth").send_keys("2")

	def input_length_place(self):
		self.driver.find_element(By.NAME, "placeLength").send_keys("4")

	def input_height_place(self):
		self.driver.find_element(By.NAME, "heightOfParkingEntry").send_keys("3")

	def input_price_per_day(self):
		self.driver.find_element(By.NAME, "pricePerDay").send_keys("100")

	def select_class_auto(self):
		Select(self.driver.find_element(By.CSS_SELECTOR, "select[name='carClass']")).select_by_value("SECOND")

	def select_type_parking(self):
		Select(self.driver.find_element(By.CSS_SELECTOR, "select[name='parkingType']")).select_by_value("UNDEGROUND")


	def doc_upload(self):
		doc = os.path.abspath('C:\Python\Parkovkapytest\DEPO.pdf')
		self.driver.find_element(By.ID, "uploadDocs").send_keys(doc)

	def image_upload(self):
		images = os.path.abspath('C:\Python\Parkovkapytest\2.png')
		self.driver.find_element(By.ID, "uploadPhoto").send_keys(images)

	def btn_send_check(self):
		self.driver.find_element(By.CSS_SELECTOR, "button.sc-kfYoZR.dCSILG").click()

	def asser_modal_moderation(self):
		return self.driver.find_element(By.CSS_SELECTOR, "div.ReactModal__Content.ReactModal__Content--after-open").get_attribute("textContent")
		"""
		if len(self.driver.find_element(By.CSS_SELECTOR, "div.ReactModal__Content.ReactModal__Content--after-open")) > 0:
			self.driver.find_element(By.CSS_SELECTOR, "button.sc-kfYoZR.dMcanQ").click()
		else:
			return "Оголошення не відправленно на модерацію"
		"""