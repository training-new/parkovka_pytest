import json

a = {
    "firstName": "Jane",
    "lastName": "Doe",
    "hobbies": ["running", "sky diving", "singing"],
    "age": 35,
    "children": [
        {
            "firstName": "Alice",
            "age": 6
        },
        {
            "firstName": "Bob",
            "age": 8
        }
    ]
}
# with open("test.json", "w") as write_file:
#     json.dump(a, write_file)
# print(type(a))

json_str = json.dumps(a, indent=4)
data = json.loads(json_str)
# print(type(data["children"]))
for item in data["children"]:
    print(item["firstName"], item["age"])

# print(type(json_str))
# data = json.loads(json_str)
# print(type(data))
# print(data)
# with open("test.json", "r") as read_file:
#     b = json.load(read_file)
#
# print(type(b))
# print(b)
