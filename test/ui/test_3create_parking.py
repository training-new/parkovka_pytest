import pytest
import time
import allure
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from pages.create_parking import Create_parking
from test.ui.test_1login import TestLogin
from data_object_model.data_login_user import login
from pages.Registr import  Login
from data_object_model.data_storage import localStorage, localSt

class TestParking:

	def setup_method(self):
		#self.driver = webdriver.Chrome(executable_path="./chromedriver.exe")
		#self.driver = webdriver.Chrome(executable_path=ChromeDriverManager().install())
		self.driver = webdriver.Chrome(ChromeDriverManager().install())
		self.parking = Create_parking(self.driver)
		self.login2 = Login(self.driver)

		self.driver.maximize_window()

	@pytest.mark.parametrize("localStorage", localStorage)
	def test_cr_parking(self, localStorage):
		self.login2.open()
		self.login2.Login_btn_1()
		self.login2.Login_btn_2()
		self.login2.Email_aut("sichkarenko96@gmail.com")
		self.login2.Password_input("123Qwe123")
		self.login2.Login_conform_btn()
		self.parking.open()
		self.driver.execute_script('window.localStorage.setItem("accessToken", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MjA4LCJyb2xlIjoiVVNFUiIsImlhdCI6MTY0MjUzNjgwMCwiZXhwIjoxNjQyNTU0ODAwfQ.JaIWOoaBuZVLQG6gM3OWmhmDOI5M_ToumnvL6rmpkVE")')
		# #self.driver.execute_script('window.localStorage.setItem("refreshToken", "[{"token":"34a58b1a-4006-4096-ad34-af6ddd2c6597","expiresAt":"2022-01-19T20:13:20.826Z"}]")')
		#self.driver.execute_script('window.localStorage.setItem("userInfo", "[{"id":208,"name":"Evgen","surname":"Sichka","email":"sichkarenko96@gmail.com","emailConfirmed":true,"phone":"+380982542111","passwordRestoring":false,"countOfParkingPlaces":"12","balance":-900}]") ')
		self.driver.execute_script('window.localStorage.setItem("userInfo", "localSt.userInfo") ')

		# self.driver.refresh()
		# time.sleep(2)
		# #self.driver.refresh()
		# time.sleep(2)
		self.parking.btn_add_parking()
		self.parking.select_options_city()
		self.parking.input_street()
		self.parking.input_house()
		self.parking.input_numb_place()
		self.parking.input_lvl_parking()
		self.parking.input_width_place()
		self.parking.input_length_place()
		self.parking.input_height_place()
		self.parking.input_price_per_day()
		self.parking.select_class_auto()
		self.parking.select_type_parking()
		self.parking.doc_upload()
		self.parking.btn_send_check()
		time.sleep(2)
		assert len(self.parking.asser_modal_moderation()) > 1
		print(self.parking.asser_modal_moderation())
		self.driver.quit()

