import time
import allure
import pytest
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from pages.booking import Booking
from data_object_model.data_user import valid_customers


class TestBooking:

	def setup_method(self):
		#self.driver = webdriver.Chrome(executable_path="./chromedriver.exe")
		#self.driver = webdriver.Chrome(executable_path=ChromeDriverManager().install())
		self.driver = webdriver.Chrome(ChromeDriverManager().install())
		self.booking = Booking(self.driver)
		self.driver.maximize_window()

	def test_booking(self):
		self.booking.open()
		self.booking.calendar_start()
		self.booking.calendar_end()
		self.booking.search_parking_button()
		time.sleep(1)
		self.booking.choose_parking()
		self.driver.switch_to.window(self.driver.window_handles[1])
		self.booking.booking_btn()
		self.booking.input_name()
		self.booking.input_brand_auto()
		self.booking.input_color()
		self.booking.input_email()
		self.booking.input_phone_number()
		self.booking.button_confirm_booking()
		assert "Запит на бронювання відправлений. Чекайте оновлення стану запиту протягом 30 хвилин" == self.booking.requst_bokk_send()
		self.booking.button_close()
		self.driver.quit()
