import time
import allure
import pytest
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from pages.Registr import Registration, Login
from data_object_model.data_user import valid_customers


class TestRegistration:

	def setup_method(self):
		#self.driver = webdriver.Chrome(executable_path="./chromedriver.exe")
		#self.driver = webdriver.Chrome(executable_path=ChromeDriverManager().install())
		self.driver = webdriver.Chrome(ChromeDriverManager().install())
		self.registration = Registration(self.driver)
		self.login = Login(self.driver)
		self.driver.maximize_window()

	@pytest.mark.parametrize("customer",valid_customers)
	def test_regisrt_user(self, customer):
		self.registration.open()
		self.registration.registration_btn1()
		self.registration.registration_btn2()
		self.registration.first_name_input(customer.firstname)
		self.registration.last_name_input(customer.lastname)
		self.registration.number_input(customer.phone)
		self.registration.email_input(customer.email)
		self.registration.password_input(customer.password)
		self.registration.condition_checkbox()
		self.registration.confirm_button()
		time.sleep(1)
		assert self.registration.number_attr() == 'Підтвердження номера'
		self.registration.quit()

	@pytest.mark.parametrize("customer",valid_customers)
	def test_registr_invalid(self, customer):
		self.registration.open()
		self.registration.registration_btn1()
		self.registration.registration_btn2()
		self.registration.first_name_input(customer.firstname)
		self.registration.last_name_input(customer.lastname)
		self.registration.number_input("")
		self.registration.email_input(customer.email)
		self.registration.password_input(customer.password)
		self.registration.condition_checkbox()
		self.registration.confirm_button()
		assert "Обов'язкове поле" == self.registration.number_text()
		print(self.registration.number_text())
		self.registration.quit()