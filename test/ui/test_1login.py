import time
import json
import pickle
from model_page.base import Application
import allure
import pytest
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from pages.Registr import  Login
from data_object_model.data_login_user import login
from model_page.localStorage import LocalStorage


class TestLogin:
USERINFO = {}
TOKEAN = {}
REFRESH = {}
	def setup_method(self):
		#self.driver = webdriver.Chrome(executable_path="./chromedriver.exe")
		#self.driver = webdriver.Chrome(executable_path=ChromeDriverManager().install())
		self.driver = webdriver.Chrome(ChromeDriverManager().install())
		self.driver.maximize_window()
		self.login = Login(self.driver)
		self.localStorage = LocalStorage(self.driver)

	@pytest.mark.parametrize("login", login)
	def test_login_user(self, login):
		self.login.open()
		self.login.Login_btn_1()
		self.login.Login_btn_2()
		self.login.Email_aut(login.email)
		self.login.Password_input(login.password)
		self.login.Login_conform_btn()
		USERINFO = self.driver.execute_script("return window.localStorage.getItem('userInfo')")
		print(USERINFO)
		# Refresh = self.localStorage.refreshToken()
		# Tok = self.localStorage.accessToken()
		time.sleep(1)
		# mew = json.dumps(self.driver.execute_script("return window.localStorage") )

		qwer = self.driver.execute_script(" return window.localStorage.getItem('userInfo')")
		print(type(qwer))
		#userInfo = {}
		for key in qwer:
			print(qwer[key])
			# print(f'{key} : {qwer[key]}')
		# print(type(qwer))
		self.login.quit()


	@pytest.mark.parametrize("login", login)
	def test_login_invalid_user(self, login):
		self.login.open()
		self.login.Login_btn_1()
		self.login.Login_btn_2()
		self.login.Email_aut(login.email)
		self.login.Password_input("gfhdfghdh")
		self.login.Login_conform_btn()
		assert self.login.email_text() == "Неправильний логин або пароль"
		print(self.login.email_text())
		self.login.quit()



