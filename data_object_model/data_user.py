from model_page.model_user import  Customer
import time

def current_time_millis():
    return int(round(time.time() * 1000))

valid_customers = [Customer(
    firstname="Adam",
    lastname="Smith",
    phone="+380982542478",
    email="adam%s@smith.me" % current_time_millis(),
    password="Qwerty123"),
                   # ...
                   ]